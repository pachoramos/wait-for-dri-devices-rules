# wait-for-dri-devices-rules

udev+systemd rules to workaround a race condition that causes a black screen at boot time on some dual graphic cards setups.

See the following links for reference:

https://wiki.archlinux.org/title/NVIDIA/Troubleshooting#Xorg_fails_during_boot,_but_otherwise_starts_fine

https://github.com/systemd/systemd/issues/25408

https://bugzilla.redhat.com/show_bug.cgi?id=2143248

